// uso de la dependencia
var express = require('express');

// inicialización del framework
var app = express();

// las peticiones que lleguen por el body elegimos cómo queremos que se parsee
// lo hacemos a partir del framework app
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//definimos las variables para el consumo de MLAB, poniendo el apikey al final como parámetro
var baseMLABURL = "https://api.mlab.com/api/1/databases/apitechubsn5ed/collections/";
var mLabAPIKey = "apiKey=YRgGUc-btbD9gqLd9kn76iUwAkhedu_g";
//Dependencia para gestionar peticiones http
var requestJson = require ('request-json');

// establecemos puerto de escucha
var port = process.env.PORT||3000;
app.listen(port);
console.log("API molona escuchando en el puerto: " + port);

// abrimos el API para petición de tipo GET con la ruta donde empezará a escuchar
// y su función manejadora (req = petición del navegador y res = repuesta)
// pasar la función manejadora como parámetro se ejecuta en modo callback

// 1ª función para ver que el API funciona
app.get("/apitechu/v1",
  function(req,res){
    console.log("GET /apitechu/v1");
    // respuesta en html
    //res.send("respuesta: Hola desde API TechU.");
    // respuesta en json
    res.send({"msg":"Hola desde APITechU"});
  }
);
// 2ª función para ver que el API. lista de usuarios
app.get("/apitechu/v1/users",
  function(req,res){
    console.log("GET /apitechu/v1/users");
    // respuesta del contenido del fichero usuarios.json (carpeta proyecto)
    //res.sendFile('./usaurios.json'); //DEPRECATED
    // __dirname: variable de entorno con el path donde se está ejecutando
     res.sendFile('usuarios.json',{root: __dirname});

    //respuesta de usaurios
    //var users = require ('./usuarios.json');
    //res.send(users);
  }
);
// 2.1ª función para ver que el API. lista de usuarios desde MLAB
app.get("/apitechu/v2/users",
  function(req,res){
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMLABURL);
    console.log("Cliente Http creado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }
    );
  }
);

// 2.1ª función para ver que el API. lista de usuarios desde MLAB con Parametros
app.get("/apitechu/v2/users/:id",
  function(req,res){
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    var httpClient = requestJson.createClient(baseMLABURL);
    console.log("Cliente Http creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        //var response = !err ? body : {
        //  "msg" : "Error obteniendo usuario."
        // }
        var response = {};
        if (err) {
          response = {
             "msg" : "Error obteniendo usuario."
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario no encontrado"
              };
              res.status(404);
            }
        }
        res.send(response);
      }
    );
  }
);

// 3ª función para ver alta de usuarios
app.post("/apitechu/v1/users",
  function (req,res){
    console.log("POST /apitechu/v1/users");
    //console.log(req.headers);
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("country is " + req.body.country);


    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };
    // users se carga a partir de un fichero json y se carga (require) como
    // un array, por eso se puede añadir un nuevo usuario .push
    var users = require ('./usuarios.json');
    users.push(newUser);

    // posibilidad de persistir en el mismo fichero
    writeUserDataToFile(users);
    console.log("Usuario guardado con éxito");
    res.send({"msg" : "Usuario guardado con éxito"});
  }
)

// 4ª función para borrar un usuario
app.delete ("/apitechu/v1/users/:id",
  function (req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    //console.log(req.params);
    console.log(req.params.id);

    // convertimos en array con la carga del fichero
    var users = require ('./usuarios.json');
    // cogemos un trozo del array en el que se ha convertido el fichero
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
)

//5º función POST utilizando todos los tipos de parametro

app.post("/apitechu/v1/monstruo/:p1/:p2",
 function (req, res) {
   console.log("Parametros");
   console.log(req.params);

   console.log("Query String");
   console.log(req.query);

   console.log("Body");
   console.log(req.body);

   console.log("Headers");
   console.log(req.headers);
 }
);

// EJERCICIO PRÁCTICA LOGIN tirando de MLAB
app.post ("/apitechu/v2/login",
  function(req,res){
      console.log("POST /apitechu/v2/login");
      var httpClient = requestJson.createClient(baseMLABURL);
      var emailUser = req.body.email;
      var passUser = req.body.password;
      var query = 'q={"email":"' + emailUser + '", "password":"'+passUser+'"}';
      var bEncontrado = false;
      var response;

      httpClient.get("user?" +  query + "&" +  mLabAPIKey,
        function(err, resMLab, body){
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length <=0){
              response = {
                "msg" : "login incorrecto"
              };
              res.status(404);
              res.send(response);
            }
            else {
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(err, resPutMlab, body){
                if (err)
                {
                  response = {
                      "msg" : "Error marcando usuario logado"
                  };
                  res.status(500);
                } else {
                  console.log ("login correcto. usuario actualizado");
                  response = {
                      "msg" : "login correcto. usuario actualizado"
                    }
                }
                res.send(response);
              });
            }
          }
        }
      );
    }
)

// EJERCICIO PRÁCTICA LOGUT tirando de MLAB
app.post ("/apitechu/v2/logout",
  function(req,res){
      console.log("POST /apitechu/v2/logout");
      var httpClient = requestJson.createClient(baseMLABURL);
      var id = req.body.id;
      var query = 'q={"id":' + id + ',"logged": true}';
      var response;

      httpClient.get("user?" +  query + "&" +  mLabAPIKey,
        function(err, resMLab, body){
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length <=0){
              response = {
                "msg" : "logout incorrecto"
              };
              res.status(404);
              res.send(response);
            }else{
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(err, resPutMlab, body){
                if (err)
                {
                  response = {
                      "msg" : "Error marcando usuario logado"
                  };
                  res.status(500);
                } else {
                  console.log ("logout correcto. usuario actualizado");
                  response = {
                      "msg" : "logout correcto. usuario actualizado"
                  };
                }
                res.send(response);
              });
            }
          }
        }
      );
    }
)

// EJERCICIO PRÁCTICA LOGIN tirando de fichero
app.post("/apitechu/v1/login",
  function (req,res){
    console.log("POST /apitechu/v1/login");

    var newUser = {
      "email" : req.body.email,
      "password" : req.body.password,
    };

    console.log("email " + req.body.email);
    console.log("password " + req.body.password);

    var users = require ('./usuarios_login.json');
    var mensaje = "Login incorrecto";
    var idUsuario;
    var encontrado = false;
    for (user of users){
      if (user.email == newUser.email && user.password == newUser.password){
        encontrado = true;
        user.logged = true;
        console.log (user);
        idUsuario = user.id;
        writeUserDataToFile(users);
      }
    }
    if (encontrado){
      mensaje = "Login correcto";
      res.send ({"mensaje" : mensaje, "idUsuario" : idUsuario});
    }
  }
)
// EJERCICIO PRÁCTICA LOGOUT tirando de fichero
app.post("/apitechu/v1/logout",
  function (req,res){
    console.log("POST /apitechu/v1/logout");

    var idUsuario = req.body.id;
    console.log("idUsuario " + req.body.id);

    var users = require ('./usuarios_login.json');
    var mensaje = "Logout incorrecto";
    var encontrado = false;
    for (user of users){
      if (user.id == idUsuario && user.logged){
        encontrado = true;
        delete user.logged;
        writeUserDataToFile(users);
      }
    }
    if (encontrado){
      mensaje = "logout correcto";
      res.send ({"mensaje" : mensaje, "idUsuario" : idUsuario});
    } else {
      res.send ({"mensaje" : mensaje});
    }
  }
)


// 1ª función API de consulta cuentas. lista de cuentas desde MLAB con Parametros
app.get("/apitechu/v2/users/:id/accounts",
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userId":' + id + '}';

    var httpClient = requestJson.createClient(baseMLABURL);
    console.log("Cliente Http creado");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
      var response = {};
        if (err) {
          response = {
             "msg" : "Error obteniendo cuentas."
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario no encontrado"
              };
              res.status(404);
            }
        }
        res.send(response);
      }
    );
  }
);



// FUNCIONES REUTILIZABLES
function writeUserDataToFile(data){
  var fs = require ('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios_login.json", jsonUserData,"utf8",
    function (err){
      if (err){
        console.log (err);
      } else {
        console.log ("Datos escritos en archivo");
      }
    }
  );
}
