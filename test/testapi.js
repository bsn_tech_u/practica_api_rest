var mocha = require('mocha');
var chai = require ('chai');
var chaihttp = require ('chai-http');

chai.use(chaihttp);

// Indicamos las aserciones que se usaran de CHAI
// Usaremos should
var should = chai.should();

var server = require('../server');

// creamos la suite de prueba para peticiones http (pruebas del api rest)
describe('First test suite',
  function(){
    it('Test that DuckDuckGo works',
        function(done){
          chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
            function(err, res)  {
              console.log("Request has ended");
              console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();
            }
          );
        }
      );
  }
);

// creamos una suite de prueba para nuestro proyecto
describe('Test de API de usuarios Tech U',
  function(){
    it('Prueba que la API de usuarios funciona OK',
        function(done){
          chai.request('http://localhost:3000')
          .get('/apitechu/v1')
          .end(
            function(err, res)  {
              console.log("Request has ended");
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde APITechU");
              done();
            }
          );
        }
      ),
      it('Prueba que la API devuelve una lista de usuarios correctos',
          function(done){
            chai.request('http://localhost:3000')
            .get('/apitechu/v1/users')
            .end(
              function(err, res)  {
                console.log("Request has ended");
                res.should.have.status(200);
                res.body.should.be.a("array");
                for (user of res.body){
                  user.should.have.property('email');
                  user.should.have.property('password');
                }
                done();
              }
            );
          }
        )
  }
);
