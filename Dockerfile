# establecemos nuestro node
FROM node

# establecemos directorio de trabajo
WORKDIR /apitechu

# establecemos lo que vamos a añadir
ADD . /apitechu

# establecemos el directorio sobre el que se expondrá el API
EXPOSE 3000

# indicamos el comando que se va a ejecutar cuando se posicione en nuestro directorio de trabajo. En este caso es arrancar
CMD ["npm", "start"]
